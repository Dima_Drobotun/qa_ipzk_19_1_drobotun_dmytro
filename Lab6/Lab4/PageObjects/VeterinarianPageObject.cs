﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab6.PageObjects
{
    public class VeterinarianPageObject : BasePageObject
    {
        public VeterinarianPageObject(IWebDriver driver) : base(driver) { }

        private By VeterinariansPageHeader = By.CssSelector("h2");
        private By LastDeleteButton = By.CssSelector("tr:last-child .deleteVet");
        private By Veterinarians = By.CssSelector("tbody > tr");
        private By VeterinarianFirstName = By.Id("firstName");
        private By VeterinarianLastName = By.Id("lastName");
        private By SaveButton = By.CssSelector(".saveVet");
        private By LastVeterinarianFullName = By.CssSelector("tr:last-child > td.vetFullName");

        [AllureStep("Get veterinarians page header text")]
        public string GetVeterinariansPageHeader()
        {
            return driver.FindElement(VeterinariansPageHeader).Text;
        }

        [AllureStep("Get current veterinarians items count")]
        public int GetVeterinariansCount()
        {
            return driver.FindElements(Veterinarians).Count;
        }

        [AllureStep("Set current veterinarians count, click on the last delete button")]
        public void DeleteLastVeterinarian()
        {
            vars["veterinarians_count"] = GetVeterinariansCount();
            driver.FindElement(LastDeleteButton).Click();
        }

        [AllureStep("Get previous veterinarians count value")]
        public int GetPreviousVeterinariansCount()
        {
            return (int) vars["veterinarians_count"];
        }

        [AllureStep("Set values, click save")]
        public void AddVeterinarian(string firstName, string lastName)
        {
            Helper.ClickAndSendKeys(driver.FindElement(VeterinarianFirstName), firstName);
            Helper.ClickAndSendKeys(driver.FindElement(VeterinarianLastName), lastName);
            driver.FindElement(SaveButton).Click();
        }

        [AllureStep("Get last veterinarian item full name value")]
        public string GetLastVeterinarianFullName()
        {
            return driver.FindElement(LastVeterinarianFullName).Text;
        }
    }
}
