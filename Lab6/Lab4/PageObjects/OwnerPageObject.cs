﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab6.PageObjects
{
    public class OwnerPageObject : BasePageObject
    {
        public OwnerPageObject(IWebDriver driver) : base(driver) {}

        private By OwnerInfoHeader = By.CssSelector("h2");
        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerPhone = By.Id("telephone");
        private By OwnerAddButton = By.CssSelector(".addOwner");
        private By LastOwnerFullName = By.CssSelector("tr:last-of-type > td.ownerFullName > a");

        [AllureStep("Get owner info page header value")]
        public string GetOwnerInfoPageHeader()
        {
            return driver.FindElement(OwnerInfoHeader).Text;
        }

        [AllureStep("Set values to fields and click Add")]
        public void AddOwner(string firstName, string lastName, string address, string city, string phone)
        {
            Helper.ClickAndSendKeys(driver.FindElement(OwnerFirstName), firstName);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerLastName), lastName);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerAddress), address);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerCity), city);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerPhone), phone);
            driver.FindElement(OwnerAddButton).Click();
        }

        [AllureStep("Get last owner element full name value")]
        public string GetLastOwnerFullName()
        {
            return driver.FindElement(LastOwnerFullName).Text;
        }
    }
}
