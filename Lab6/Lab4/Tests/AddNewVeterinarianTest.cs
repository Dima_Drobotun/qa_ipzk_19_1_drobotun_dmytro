using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactory;
using Lab6.PageComponents;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class AddNewVeterinarianTest : BaseTest
    {
        [Test, Description("This test checks that veterinarian was created correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void AddNewVeterinarian()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinarianAddPage();
            veterinarianPage.AddVeterinarian("Dmytro", "Drobotun");
            Helper.Wait();
            Assert.That(veterinarianPage.GetLastVeterinarianFullName(), Is.EqualTo("Dmytro Drobotun"));
        }
    }
}