using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class AddNewSpecialtyTest : BaseTest
    {
        [Test, Description("This test checks that specialty was created correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void AddNewSpecialty()
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            specialtyPage.AddSpecialty("Dmytro");
            Helper.Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo("Dmytro"));           
        }
    }
}