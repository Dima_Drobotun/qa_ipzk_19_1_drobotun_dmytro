using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class DeleteVeterinarianTest : BaseTest
    {
        [Test, Description("This test checks that veterinarian was deleted correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void DeleteVeterinarian()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinariansAllPage();
            Helper.Wait();
            veterinarianPage.DeleteLastVeterinarian();
            Helper.Wait(500);
            Assert.That(veterinarianPage.GetVeterinariansCount(),
                Is.EqualTo(veterinarianPage.GetPreviousVeterinariansCount() - 1));
        }
    }
}
