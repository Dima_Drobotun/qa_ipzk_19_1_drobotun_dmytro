using NUnit.Framework;
using Lab6.PageObjects;
using Lab6.Tests;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class EditPetTypeTest : BaseTest
    {
        [Test, Description("This test checks that pet type was edited correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        public void EditPetType()
        {
            TabsComponent tabs = Components.Tabs;
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            tabs.OpenPetTypesAllPage();
            Helper.Wait();
            petTypePage.EditPetType("Zubr");
            Helper.Wait(1000);
            Assert.That(petTypePage.GetFirstPetTypeName(), Is.EqualTo("Zubr"));
        }
    }
}