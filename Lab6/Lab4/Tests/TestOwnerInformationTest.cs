using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class TestOwnerInformationTest : BaseTest
    {
        [Test, Description("This test checks that owners page was loaded correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Owner")]
        public void TestOwnerInformation()
        {
            TabsComponent tabs = Components.Tabs;
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            tabs.OpenAllOwnersPage();
            Helper.Wait(1000);
            tabs.OpenFirstOwnerInfoPage();
            Helper.Wait(500);
            Assert.That(ownerPage.GetOwnerInfoPageHeader(), Is.EqualTo("Owner Information"));
        }
    }
}