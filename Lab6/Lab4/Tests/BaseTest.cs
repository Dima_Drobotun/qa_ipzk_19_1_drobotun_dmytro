﻿using Allure.Commons;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;

namespace Lab6.Tests
{
    public abstract class BaseTest
    {
        public static IWebDriver driver;
        Dictionary<string, object> additionalSelenoidCapabilities = new Dictionary<string, object>();

        [SetUp]
        public void Setup()
        {
            const string url = "http://20.50.171.10:8080/";
            const string remoteUrl = "http://localhost:4444/wd/hub";
            const int windowWidth = 1366;
            const int windowHeight = 728;

            additionalSelenoidCapabilities["name"] = "Simple test";
            additionalSelenoidCapabilities["enableVNC"] = true;
            additionalSelenoidCapabilities["enableVideo"] = true;
            var chrome_options = new ChromeOptions();
            chrome_options.AddAdditionalOption("selenoid:options", additionalSelenoidCapabilities);
            driver = new RemoteWebDriver(new Uri(remoteUrl), chrome_options.ToCapabilities());
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Size = new System.Drawing.Size(windowWidth, windowHeight);
        }

        [TearDown]
        protected void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                var filename = TestContext.CurrentContext.Test.MethodName + "_screenshot_" + DateTime.Now.Ticks + ".png";
                var path = @"E:\QA\Lab6\Lab4\TestResults";
                screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
                AllureLifecycle.Instance.AddAttachment(filename, "image/png", path);
            }
            driver.Quit();
        }
    }
}
