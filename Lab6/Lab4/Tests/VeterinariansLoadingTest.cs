using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class VeterinariansLoadingTest : BaseTest
    {
        [Test, Description("This test checks that veterinarians page was loaded correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void VeterinariansLoading()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinariansAllPage();
            Helper.Wait();
            Assert.That(veterinarianPage.GetVeterinariansPageHeader(), Is.EqualTo("Veterinarians"));
        }
    }
}