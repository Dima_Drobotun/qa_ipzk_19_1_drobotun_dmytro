using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class EditSpecialtyTest : BaseTest
    {
        [Test, Description("This test checks that specialty was edited correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void EditSpecialty()
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            specialtyPage.EditLastSpecialtyName("Proggrammered");
            Helper.Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo("Proggrammered"));
        }
    }
}