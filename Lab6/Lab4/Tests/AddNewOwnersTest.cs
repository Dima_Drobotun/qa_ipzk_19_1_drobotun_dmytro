using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class AddNewOwnersTest : BaseTest
    {
        [Test, Description("This test checks that owner was created correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Owner")]
        public void AddNewOwner()
        {
            TabsComponent tabs = Components.Tabs;
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            tabs.OpenAddOwnerPage();
            ownerPage.AddOwner("Dmytro", "Drobotun", "Nebesnaya Sotnya", "Zhytomyr", "0643062310");
            Helper.Wait();
            Assert.That(ownerPage.GetLastOwnerFullName(), Is.EqualTo("Dmytro Drobotun"));
        }
    }
}