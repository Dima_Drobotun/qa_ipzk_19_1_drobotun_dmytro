﻿using OpenQA.Selenium;
using System.Threading;

namespace Lab6
{
    class Helper
    {
        public static void ClickAndSendKeys(IWebElement webElement, string value)
        {
            webElement.Click();
            webElement.SendKeys(value);
        }

        public static void ClickClearAndSendKeys(IWebElement webElement, string value)
        {
            webElement.Click();
            webElement.Clear();
            webElement.SendKeys(value);
        }

        public static void Wait(int time = 1500)
        {
            Thread.Sleep(time);
        }
    }
}
