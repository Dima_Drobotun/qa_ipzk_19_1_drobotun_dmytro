﻿using Lab6.PageObjects;
using Lab6.Tests;

namespace Lab6.PageFactory
{
    public static class Pages
    {
        public static OwnerPageObject OwnerPage => new OwnerPageObject(BaseTest.driver);
        public static PetTypePageObject PetTypePage => new PetTypePageObject(BaseTest.driver);
        public static SpecialtyPageObject SpecialtyPage => new SpecialtyPageObject(BaseTest.driver);
        public static VeterinarianPageObject VeterinarianPage => new VeterinarianPageObject(BaseTest.driver);
    }
}
