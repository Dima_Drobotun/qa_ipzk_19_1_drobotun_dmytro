﻿using Lab6.PageComponents;
using Lab6.Tests;

namespace Lab6.PageFactory
{
    public static class Components
    {
        public static TabsComponent Tabs => new TabsComponent(BaseTest.driver);
    }
}
