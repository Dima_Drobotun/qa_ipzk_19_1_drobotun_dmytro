﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab7.PageObjects
{
    public class SpecialtyPageObject : BasePageObject
    {
        public SpecialtyPageObject(IWebDriver driver) : base(driver) { }

        private By SpecialtiesHeader = By.CssSelector("h2");
        private By LastSpecialtyEditButton = By.CssSelector("tr:last-child .editSpecialty");
        private By SpecialtyName = By.Id("name");
        private By UpdateButton = By.CssSelector(".updateSpecialty");
        private By LastSpecialtyName = By.CssSelector("tr:last-child input");
        private By AddButton = By.CssSelector(".addSpecialty");
        private By SaveButton = By.CssSelector(".btn:nth-child(3)");

        [AllureStep("Get specialties page header text")]
        public string GetSpecialtiesPageHeader()
        {
            return driver.FindElement(SpecialtiesHeader).Text;
        }

        [AllureStep("Click on the last specialty name, set values, click update")]
        public void EditLastSpecialtyName(string name)
        {
            driver.FindElement(LastSpecialtyEditButton).Click();
            Helper.ClickClearAndSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(UpdateButton).Click();
        }

        [AllureStep("Get last specialty item name value")]
        public string GetLastSpecialtyName()
        {
            return driver.FindElement(LastSpecialtyName).GetAttribute("value");
        }

        [AllureStep("Click add, set values, click save")]
        public void AddSpecialty(string name)
        {
            driver.FindElement(AddButton).Click();
            Helper.ClickAndSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(SaveButton).Click();      
        }
    }
}
