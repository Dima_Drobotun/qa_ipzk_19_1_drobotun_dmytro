﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab7.PageObjects
{
    public class PetTypePageObject : BasePageObject
    {
        public PetTypePageObject(IWebDriver driver) : base(driver) { }

        private By FirstEditButton = By.CssSelector("tr:nth-child(1) .editPet");
        private By PetTypeName = By.Id("name");
        private By SaveButton = By.CssSelector(".updatePetType");
        private By FirstPetTypeName = By.CssSelector("tr:first-child input");
        private By FirstDeleteButton = By.CssSelector("tr:nth-child(1) .deletePet");
        private By PetTypes = By.CssSelector("tbody > tr");

        [AllureStep("Click on the first edit button, set values and click save")]
        public void EditPetType(string name)
        {
            driver.FindElement(FirstEditButton).Click();
            Helper.ClickClearAndSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(SaveButton).Click();
        }

        [AllureStep("Get first pet type name value")]
        public string GetFirstPetTypeName()
        {
            return driver.FindElement(FirstPetTypeName).GetAttribute("value");
        }

        [AllureStep("Get all pet types count")]
        public int GetPetTypesCount()
        {
            return driver.FindElements(PetTypes).Count;
        }

        [AllureStep("Set current pet types count, click on the first delete button")]
        public void DeleteFirstPetType()
        {
            vars["pet_types_count"] = GetPetTypesCount();
            driver.FindElement(FirstDeleteButton).Click();
        }

        [AllureStep("Get previous pet types count")]
        public int GetPreviousPetTypesCount()
        {
            return (int) vars["pet_types_count"];
        }
    }
}
