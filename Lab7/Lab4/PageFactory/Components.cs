﻿using Lab7.PageComponents;
using Lab7.Tests;

namespace Lab7.PageFactory
{
    public static class Components
    {
        public static TabsComponent Tabs => new TabsComponent(BaseTest.driver);
    }
}
