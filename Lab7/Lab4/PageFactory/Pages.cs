﻿using Lab7.PageObjects;
using Lab7.Tests;

namespace Lab7.PageFactory
{
    public static class Pages
    {
        public static OwnerPageObject OwnerPage => new OwnerPageObject(BaseTest.driver);
        public static PetTypePageObject PetTypePage => new PetTypePageObject(BaseTest.driver);
        public static SpecialtyPageObject SpecialtyPage => new SpecialtyPageObject(BaseTest.driver);
        public static VeterinarianPageObject VeterinarianPage => new VeterinarianPageObject(BaseTest.driver);
    }
}
