using NUnit.Framework;
using Lab7.PageObjects;
using Lab7.Tests;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class EditPetTypeTest : BaseTest
    {
        [Test, Description("This test checks that pet type was edited correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [TestCase("Zubr")]
        [TestCase("Cat")]
        [TestCase("Cow")]
        [AllureSuite("Pet type")]
        public void EditPetType(string name)
        {
            TabsComponent tabs = Components.Tabs;
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            tabs.OpenPetTypesAllPage();
            Helper.Wait();
            petTypePage.EditPetType(name);
            Helper.Wait(1000);
            Assert.That(petTypePage.GetFirstPetTypeName(), Is.EqualTo(name));
        }
    }
}