using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class TestOwnerInformationTest : BaseTest
    {
        [Test, Description("This test checks that owners page was loaded correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [TestCase("Owner Information")]
        [TestCase("Owner Info")]
        [TestCase("Owner Inform")]
        [AllureSuite("Owner")]
        public void TestOwnerInformation(string text)
        {
            TabsComponent tabs = Components.Tabs;
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            tabs.OpenAllOwnersPage();
            Helper.Wait(1000);
            tabs.OpenFirstOwnerInfoPage();
            Helper.Wait(500);
            Assert.That(ownerPage.GetOwnerInfoPageHeader(), Is.EqualTo(text));
        }
    }
}