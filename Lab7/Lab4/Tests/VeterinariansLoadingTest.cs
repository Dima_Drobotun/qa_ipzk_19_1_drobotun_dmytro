using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class VeterinariansLoadingTest : BaseTest
    {
        static object[] TestData = { "Veterinarians", "Veterans", "Vets" };
        [Test, Description("This test checks that veterinarians page was loaded correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        [TestCaseSource(nameof(TestData))]
        public void VeterinariansLoading(string text)
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinariansAllPage();
            Helper.Wait();
            Assert.That(veterinarianPage.GetVeterinariansPageHeader(), Is.EqualTo(text));
        }
    }
}