using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class AddNewSpecialtyTest : BaseTest
    {
        static object[] TestData = { "Dmytro", "Ivan", "Petro" };

        [Test, Description("This test checks that specialty was created correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        [TestCaseSource(nameof(TestData))]
        public void AddNewSpecialty(string name)
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            specialtyPage.AddSpecialty(name);
            Helper.Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo(name));           
        }
    }
}