using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class SpecialtiesLoadingTest : BaseTest
    {
        [Test, Description("This test checks that specialties page was loaded correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void SpecialtiesLoading([Values("Specialties", "Specialty", "Spec")] string text)
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            Assert.That(specialtyPage.GetSpecialtiesPageHeader(), Is.EqualTo(text));
        }
    }
}