using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class DeletePetTypeTest : BaseTest
    {
        [Test, Description("This test checks that pet type was deleted correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        public void DeletePetType()
        {
            TabsComponent tabs = Components.Tabs;
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            tabs.OpenPetTypesAllPage();
            Helper.Wait(1000);
            petTypePage.DeleteFirstPetType();
            Helper.Wait(500);
            Assert.That(petTypePage.GetPetTypesCount(), Is.EqualTo(petTypePage.GetPreviousPetTypesCount() - 1));
        }
    }
}