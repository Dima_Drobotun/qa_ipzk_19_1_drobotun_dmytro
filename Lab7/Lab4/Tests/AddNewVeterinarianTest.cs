using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactory;
using Lab7.PageComponents;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class AddNewVeterinarianTest : BaseTest
    {
        [Test, Description("This test checks that veterinarian was created correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void AddNewVeterinarian([Values("Dmytro", "Ivan", "Petro")] string firstName)
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinarianAddPage();
            veterinarianPage.AddVeterinarian(firstName, "Drobotun");
            Helper.Wait();
            Assert.That(veterinarianPage.GetLastVeterinarianFullName(), Is.EqualTo($"{firstName} Drobotun"));
        }
    }
}