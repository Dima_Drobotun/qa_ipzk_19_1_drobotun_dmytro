using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class EditSpecialtyTest : BaseTest
    {
        static object[] TestData = { "Proggrammered", "Calf", "Lion" };
        [Test, Description("This test checks that specialty was edited correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        [TestCaseSource(nameof(TestData))]
        public void EditSpecialty(string name)
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            specialtyPage.EditLastSpecialtyName(name);
            Helper.Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo(name));
        }
    }
}