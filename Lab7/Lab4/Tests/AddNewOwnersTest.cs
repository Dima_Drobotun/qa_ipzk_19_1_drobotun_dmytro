using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class AddNewOwnersTest : BaseTest
    {
        [Test, Description("This test checks that owner was created correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [TestCase("Dmytro")]
        [TestCase("Ivan")]
        [TestCase("Petro")]
        [AllureSuite("Owner")]
        public void AddNewOwner(string firstName)
        {
            TabsComponent tabs = Components.Tabs;
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            tabs.OpenAddOwnerPage();
            ownerPage.AddOwner(firstName, "Drobotun", "Nebesnaya Sotnya", "Zhytomyr", "0643062310");
            Helper.Wait();
            Assert.That(ownerPage.GetLastOwnerFullName(), Is.EqualTo($"{firstName} Drobotun"));
        }
    }
}