using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactory;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace Lab7
{
    [TestFixture]
    [Parallelizable]
    [AllureNUnit]
    public class DeleteVeterinarianTest : BaseTest
    {
        [Test, Description("This test checks that veterinarian was deleted correctly")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void DeleteVeterinarian()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinariansAllPage();
            Helper.Wait();
            veterinarianPage.DeleteLastVeterinarian();
            Helper.Wait(500);
            Assert.That(veterinarianPage.GetVeterinariansCount(),
                Is.EqualTo(veterinarianPage.GetPreviousVeterinariansCount() - 1));
        }
    }
}
