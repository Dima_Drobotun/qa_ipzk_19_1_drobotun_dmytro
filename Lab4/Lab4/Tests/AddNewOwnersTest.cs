using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class AddNewOwnersTest : BaseTest
    {
        [Test]
        public void AddNewOwner()
        {
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            ownerPage.OpenAddOwnerPage();
            ownerPage.AddOwner("Dmytro", "Drobotun", "Nebesnaya Sotnya", "Zhytomyr", "0643062310");
            Wait();
            Assert.That(ownerPage.GetLastOwnerFullName(), Is.EqualTo("Dmytro Drobotun"));
        }
    }
}