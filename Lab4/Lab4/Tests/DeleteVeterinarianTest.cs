using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class DeleteVeterinarianTest : BaseTest
    {
        [Test]
        public void DeleteVeterinarian()
        {
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            veterinarianPage.OpenVeterinariansAllPage();
            Wait();
            veterinarianPage.DeleteLastVeterinarian();
            Wait(500);
            Assert.That(veterinarianPage.GetVeterinariansCount(),
                Is.EqualTo(veterinarianPage.GetPreviousVeterinariansCount() - 1));
        }
    }
}
