using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class TestOwnerInformationTest : BaseTest
    {
        [Test]
        public void TestOwnerInformation()
        {
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            ownerPage.OpenAllOwnersPage();
            Wait(1000);
            ownerPage.OpenFirstIwnerInfoPage();
            Wait(500);
            Assert.That(ownerPage.GetOwnerInfoPageHeader(), Is.EqualTo("Owner Information"));
        }
    }
}