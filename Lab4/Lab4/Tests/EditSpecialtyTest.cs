using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class EditSpecialtyTest : BaseTest
    {
        [Test]
        public void EditSpecialty()
        {
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            specialtyPage.OpenSpecialtiesPage();
            Wait();
            specialtyPage.EditLastSpecialtyName("Proggrammered");
            Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo("Proggrammered"));
        }
    }
}