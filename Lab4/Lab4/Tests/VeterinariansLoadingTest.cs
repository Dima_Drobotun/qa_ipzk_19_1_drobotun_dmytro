using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class VeterinariansLoadingTest : BaseTest
    {
        [Test]
        public void VeterinariansLoading()
        {
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            veterinarianPage.OpenVeterinariansAllPage();
            Wait();
            Assert.That(veterinarianPage.GetVeterinariansPageHeader(), Is.EqualTo("Veterinarians"));
        }
    }
}