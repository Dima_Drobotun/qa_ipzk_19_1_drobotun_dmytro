using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class SpecialtiesLoadingTest : BaseTest
    {
        [Test]
        public void SpecialtiesLoading()
        {
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            specialtyPage.OpenSpecialtiesPage();
            Wait();
            Assert.That(specialtyPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
        }
    }
}