﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace Lab4.Tests
{
    public class BaseTest
    {
        protected IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            const string url = "http://20.50.171.10:8080/";
            const int windowWidth = 1366;
            const int windowHeight = 728;

            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Size = new System.Drawing.Size(windowWidth, windowHeight);
        }

        [TearDown]
        protected void TearDown()
        {
            driver.Quit();
        }

        public void Wait(int time = 1500)
        {
            Thread.Sleep(time);
        }
    }
}
