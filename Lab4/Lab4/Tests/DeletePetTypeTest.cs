using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class DeletePetTypeTest : BaseTest
    {
        [Test]
        public void DeletePetType()
        {
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            petTypePage.OpenPetTypesAllPage();
            Wait(1000);
            petTypePage.DeleteFirstPetType();
            Wait(500);
            Assert.That(petTypePage.GetPetTypesCount(), Is.EqualTo(petTypePage.GetPreviousPetTypesCount() - 1));
        }
    }
}