using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class AddNewSpecialtyTest : BaseTest
    {
        [Test]
        public void AddNewSpecialty()
        {
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            specialtyPage.OpenSpecialtiesPage();
            Wait();
            specialtyPage.AddSpecialty("Dmytro");
            Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo("Dmytro"));           
        }
    }
}