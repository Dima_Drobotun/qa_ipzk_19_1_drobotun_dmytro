using NUnit.Framework;
using Lab4.PageObjects;
using Lab4.Tests;

namespace Lab4
{
    [TestFixture]
    public class EditPetTypeTest : BaseTest
    {
        [Test]
        public void EditPetType()
        {
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            petTypePage.OpenPetTypesAllPage();
            Wait();
            petTypePage.EditPetType("Zubr");
            Wait(1000);
            Assert.That(petTypePage.GetFirstPetTypeName(), Is.EqualTo("Zubr"));
        }
    }
}