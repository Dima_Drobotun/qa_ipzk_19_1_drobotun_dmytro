using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class AddNewVeterinarianTest : BaseTest
    {
        [Test]
        public void AddNewVeterinarian()
        {
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            veterinarianPage.OpenVeterinarianAddPage();
            veterinarianPage.AddVeterinarian("Dmytro", "Drobotun");
            Wait();
            Assert.That(veterinarianPage.GetLastVeterinarianFullName(), Is.EqualTo("Dmytro Drobotun"));
        }
    }
}