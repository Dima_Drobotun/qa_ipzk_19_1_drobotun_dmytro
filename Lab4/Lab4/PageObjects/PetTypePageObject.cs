﻿using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class PetTypePageObject : BasePageObject
    {
        public PetTypePageObject(IWebDriver driver) : base(driver) { }

        private By PetTypeTab = By.CssSelector("li:nth-child(4) > a");
        private By FirstEditButton = By.CssSelector("tr:nth-child(1) .editPet");
        private By PetTypeName = By.Id("name");
        private By SaveButton = By.CssSelector(".updatePetType");
        private By FirstPetTypeName = By.CssSelector("tr:first-child input");
        private By FirstDeleteButton = By.CssSelector("tr:nth-child(1) .deletePet");
        private By PetTypes = By.CssSelector("tbody > tr");

        public void OpenPetTypesAllPage()
        {
            driver.FindElement(PetTypeTab).Click();
        }

        public void EditPetType(string name)
        {
            driver.FindElement(FirstEditButton).Click();
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).Clear();
            driver.FindElement(PetTypeName).SendKeys(name);
            driver.FindElement(SaveButton).Click();
        }

        public string GetFirstPetTypeName()
        {
            return driver.FindElement(FirstPetTypeName).GetAttribute("value");
        }

        public int GetPetTypesCount()
        {
            return driver.FindElements(PetTypes).Count;
        }

        public void DeleteFirstPetType()
        {
            vars["pet_types_count"] = GetPetTypesCount();
            driver.FindElement(FirstDeleteButton).Click();
        }

        public int GetPreviousPetTypesCount()
        {
            return (int) vars["pet_types_count"];
        }
    }
}
