﻿using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class OwnerPageObject : BasePageObject
    {
        public OwnerPageObject(IWebDriver driver) : base(driver) {}

        private By OwnerTab = By.CssSelector(".ownerTab");
        private By AllOwnersItem = By.CssSelector(".open li:nth-child(1) > a");
        private By FirstOwnerLink = By.CssSelector("tr:first-of-type > td.ownerFullName > a");
        private By OwnerInfoHeader = By.CssSelector("h2");
        private By AddOwnerItem = By.CssSelector(".open li:nth-child(2) span:nth-child(2)");
        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerPhone = By.Id("telephone");
        private By OwnerAddButton = By.CssSelector(".addOwner");
        private By LastOwnerFullName = By.CssSelector("tr:last-of-type > td.ownerFullName > a");

        public void OpenAllOwnersPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(AllOwnersItem).Click();
        }

        public void OpenFirstIwnerInfoPage()
        {
            driver.FindElement(FirstOwnerLink).Click();
        }

        public string GetOwnerInfoPageHeader()
        {
            return driver.FindElement(OwnerInfoHeader).Text;
        }

        public void OpenAddOwnerPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(AddOwnerItem).Click();
        }

        public void AddOwner(string firstName, string lastName, string address, string city, string phone)
        {
            driver.FindElement(OwnerFirstName).Click();
            driver.FindElement(OwnerFirstName).SendKeys(firstName);
            driver.FindElement(OwnerLastName).Click();
            driver.FindElement(OwnerLastName).SendKeys(lastName);
            driver.FindElement(OwnerAddress).Click();
            driver.FindElement(OwnerAddress).SendKeys(address);
            driver.FindElement(OwnerCity).Click();
            driver.FindElement(OwnerCity).SendKeys(city);
            driver.FindElement(OwnerPhone).Click();
            driver.FindElement(OwnerPhone).SendKeys(phone);
            driver.FindElement(OwnerAddButton).Click();
        }

        public string GetLastOwnerFullName()
        {
            return driver.FindElement(LastOwnerFullName).Text;
        }
    }
}
