﻿using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class VeterinarianPageObject : BasePageObject
    {
        public VeterinarianPageObject(IWebDriver driver) : base(driver) { }

        private By VeterinariansTab = By.CssSelector(".vetsTab");
        private By VeterinariansAllItem = By.CssSelector(".open li:nth-child(1) span:nth-child(2)");
        private By VeterinariansPageHeader = By.CssSelector("h2");
        private By LastDeleteButton = By.CssSelector("tr:last-child .deleteVet");
        private By Veterinarians = By.CssSelector("tbody > tr");
        private By VeterinariansAddItem = By.CssSelector(".open li:nth-child(2) > a");
        private By VeterinarianFirstName = By.Id("firstName");
        private By VeterinarianLastName = By.Id("lastName");
        private By SaveButton = By.CssSelector(".saveVet");
        private By LastVeterinarianFullName = By.CssSelector("tr:last-child > td.vetFullName");

        public void OpenVeterinariansAllPage()
        {
            driver.FindElement(VeterinariansTab).Click();
            driver.FindElement(VeterinariansAllItem).Click();
        }

        public string GetVeterinariansPageHeader()
        {
            return driver.FindElement(VeterinariansPageHeader).Text;
        }

        public int GetVeterinariansCount()
        {
            return driver.FindElements(Veterinarians).Count;
        }

        public void DeleteLastVeterinarian()
        {
            vars["veterinarians_count"] = GetVeterinariansCount();
            driver.FindElement(LastDeleteButton).Click();
        }

        public int GetPreviousVeterinariansCount()
        {
            return (int) vars["veterinarians_count"];
        }

        public void OpenVeterinarianAddPage()
        {
            driver.FindElement(VeterinariansTab).Click();
            driver.FindElement(VeterinariansAddItem).Click();
        }

        public void AddVeterinarian(string firstName, string lastName)
        {
            driver.FindElement(VeterinarianFirstName).Click();
            driver.FindElement(VeterinarianFirstName).SendKeys(firstName);
            driver.FindElement(VeterinarianLastName).Click();
            driver.FindElement(VeterinarianLastName).SendKeys(lastName);
            driver.FindElement(SaveButton).Click();
        }

        public string GetLastVeterinarianFullName()
        {
            return driver.FindElement(LastVeterinarianFullName).Text;
        }
    }
}
