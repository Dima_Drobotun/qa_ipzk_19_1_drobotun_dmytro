﻿using OpenQA.Selenium;

namespace Lab5.PageObjects
{
    public class OwnerPageObject : BasePageObject
    {
        public OwnerPageObject(IWebDriver driver) : base(driver) {}

        private By OwnerInfoHeader = By.CssSelector("h2");
        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerPhone = By.Id("telephone");
        private By OwnerAddButton = By.CssSelector(".addOwner");
        private By LastOwnerFullName = By.CssSelector("tr:last-of-type > td.ownerFullName > a");

        public string GetOwnerInfoPageHeader()
        {
            return driver.FindElement(OwnerInfoHeader).Text;
        }

        public void AddOwner(string firstName, string lastName, string address, string city, string phone)
        {
            Helper.ClickAndSendKeys(driver.FindElement(OwnerFirstName), firstName);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerLastName), lastName);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerAddress), address);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerCity), city);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerPhone), phone);
            driver.FindElement(OwnerAddButton).Click();
        }

        public string GetLastOwnerFullName()
        {
            return driver.FindElement(LastOwnerFullName).Text;
        }
    }
}
