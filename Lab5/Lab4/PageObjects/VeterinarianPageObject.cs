﻿using OpenQA.Selenium;

namespace Lab5.PageObjects
{
    public class VeterinarianPageObject : BasePageObject
    {
        public VeterinarianPageObject(IWebDriver driver) : base(driver) { }

        private By VeterinariansPageHeader = By.CssSelector("h2");
        private By LastDeleteButton = By.CssSelector("tr:last-child .deleteVet");
        private By Veterinarians = By.CssSelector("tbody > tr");
        private By VeterinarianFirstName = By.Id("firstName");
        private By VeterinarianLastName = By.Id("lastName");
        private By SaveButton = By.CssSelector(".saveVet");
        private By LastVeterinarianFullName = By.CssSelector("tr:last-child > td.vetFullName");

        public string GetVeterinariansPageHeader()
        {
            return driver.FindElement(VeterinariansPageHeader).Text;
        }

        public int GetVeterinariansCount()
        {
            return driver.FindElements(Veterinarians).Count;
        }

        public void DeleteLastVeterinarian()
        {
            vars["veterinarians_count"] = GetVeterinariansCount();
            driver.FindElement(LastDeleteButton).Click();
        }

        public int GetPreviousVeterinariansCount()
        {
            return (int) vars["veterinarians_count"];
        }

        public void AddVeterinarian(string firstName, string lastName)
        {
            Helper.ClickAndSendKeys(driver.FindElement(VeterinarianFirstName), firstName);
            Helper.ClickAndSendKeys(driver.FindElement(VeterinarianLastName), lastName);
            driver.FindElement(SaveButton).Click();
        }

        public string GetLastVeterinarianFullName()
        {
            return driver.FindElement(LastVeterinarianFullName).Text;
        }
    }
}
