﻿using OpenQA.Selenium;

namespace Lab5.PageObjects
{
    public class SpecialtyPageObject : BasePageObject
    {
        public SpecialtyPageObject(IWebDriver driver) : base(driver) { }

        private By SpecialtiesHeader = By.CssSelector("h2");
        private By LastSpecialtyEditButton = By.CssSelector("tr:last-child .editSpecialty");
        private By SpecialtyName = By.Id("name");
        private By UpdateButton = By.CssSelector(".updateSpecialty");
        private By LastSpecialtyName = By.CssSelector("tr:last-child input");
        private By AddButton = By.CssSelector(".addSpecialty");
        private By SaveButton = By.CssSelector(".btn:nth-child(3)");

        public string GetSpecialtiesPageHeader()
        {
            return driver.FindElement(SpecialtiesHeader).Text;
        }

        public void EditLastSpecialtyName(string name)
        {
            driver.FindElement(LastSpecialtyEditButton).Click();
            Helper.ClickClearAndSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(UpdateButton).Click();
        }

        public string GetLastSpecialtyName()
        {
            return driver.FindElement(LastSpecialtyName).GetAttribute("value");
        }

        public void AddSpecialty(string name)
        {
            driver.FindElement(AddButton).Click();
            Helper.ClickAndSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(SaveButton).Click();      
        }
    }
}
