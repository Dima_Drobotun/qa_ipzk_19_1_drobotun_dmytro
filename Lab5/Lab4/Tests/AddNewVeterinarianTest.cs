using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactory;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class AddNewVeterinarianTest : BaseTest
    {
        [Test]
        public void AddNewVeterinarian()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinarianAddPage();
            veterinarianPage.AddVeterinarian("Dmytro", "Drobotun");
            Helper.Wait();
            Assert.That(veterinarianPage.GetLastVeterinarianFullName(), Is.EqualTo("Dmytro Drobotun"));
        }
    }
}