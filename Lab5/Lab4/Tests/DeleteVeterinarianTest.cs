using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class DeleteVeterinarianTest : BaseTest
    {
        [Test]
        public void DeleteVeterinarian()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinariansAllPage();
            Helper.Wait();
            veterinarianPage.DeleteLastVeterinarian();
            Helper.Wait(500);
            Assert.That(veterinarianPage.GetVeterinariansCount(),
                Is.EqualTo(veterinarianPage.GetPreviousVeterinariansCount() - 1));
        }
    }
}
