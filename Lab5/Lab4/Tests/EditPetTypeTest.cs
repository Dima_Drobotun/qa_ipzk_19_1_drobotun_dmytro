using NUnit.Framework;
using Lab5.PageObjects;
using Lab5.Tests;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class EditPetTypeTest : BaseTest
    {
        [Test]
        public void EditPetType()
        {
            TabsComponent tabs = Components.Tabs;
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            tabs.OpenPetTypesAllPage();
            Helper.Wait();
            petTypePage.EditPetType("Zubr");
            Helper.Wait(1000);
            Assert.That(petTypePage.GetFirstPetTypeName(), Is.EqualTo("Zubr"));
        }
    }
}