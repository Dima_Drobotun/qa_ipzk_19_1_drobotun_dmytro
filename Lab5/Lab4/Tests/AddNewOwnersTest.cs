using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class AddNewOwnersTest : BaseTest
    {
        [Test]
        public void AddNewOwner()
        {
            TabsComponent tabs = Components.Tabs;
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            tabs.OpenAddOwnerPage();
            ownerPage.AddOwner("Dmytro", "Drobotun", "Nebesnaya Sotnya", "Zhytomyr", "0643062310");
            Helper.Wait();
            Assert.That(ownerPage.GetLastOwnerFullName(), Is.EqualTo("Dmytro Drobotun"));
        }
    }
}