using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class TestOwnerInformationTest : BaseTest
    {
        [Test]
        public void TestOwnerInformation()
        {
            TabsComponent tabs = Components.Tabs;
            OwnerPageObject ownerPage = new OwnerPageObject(driver);
            tabs.OpenAllOwnersPage();
            Helper.Wait(1000);
            tabs.OpenFirstIwnerInfoPage();
            Helper.Wait(500);
            Assert.That(ownerPage.GetOwnerInfoPageHeader(), Is.EqualTo("Owner Information"));
        }
    }
}