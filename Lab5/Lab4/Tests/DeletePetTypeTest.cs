using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class DeletePetTypeTest : BaseTest
    {
        [Test]
        public void DeletePetType()
        {
            TabsComponent tabs = Components.Tabs;
            PetTypePageObject petTypePage = new PetTypePageObject(driver);
            tabs.OpenPetTypesAllPage();
            Helper.Wait(1000);
            petTypePage.DeleteFirstPetType();
            Helper.Wait(500);
            Assert.That(petTypePage.GetPetTypesCount(), Is.EqualTo(petTypePage.GetPreviousPetTypesCount() - 1));
        }
    }
}