using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class SpecialtiesLoadingTest : BaseTest
    {
        [Test]
        public void SpecialtiesLoading()
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            Assert.That(specialtyPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
        }
    }
}