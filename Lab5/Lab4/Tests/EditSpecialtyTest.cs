using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class EditSpecialtyTest : BaseTest
    {
        [Test]
        public void EditSpecialty()
        {
            TabsComponent tabs = Components.Tabs;
            SpecialtyPageObject specialtyPage = new SpecialtyPageObject(driver);
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            specialtyPage.EditLastSpecialtyName("Proggrammered");
            Helper.Wait();
            Assert.That(specialtyPage.GetLastSpecialtyName(), Is.EqualTo("Proggrammered"));
        }
    }
}