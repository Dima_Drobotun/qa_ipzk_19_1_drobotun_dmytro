using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactory;

namespace Lab5
{
    [TestFixture]
    public class VeterinariansLoadingTest : BaseTest
    {
        [Test]
        public void VeterinariansLoading()
        {
            TabsComponent tabs = Components.Tabs;
            VeterinarianPageObject veterinarianPage = new VeterinarianPageObject(driver);
            tabs.OpenVeterinariansAllPage();
            Helper.Wait();
            Assert.That(veterinarianPage.GetVeterinariansPageHeader(), Is.EqualTo("Veterinarians"));
        }
    }
}