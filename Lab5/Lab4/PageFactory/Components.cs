﻿using Lab5.PageComponents;
using Lab5.Tests;

namespace Lab5.PageFactory
{
    public static class Components
    {
        public static TabsComponent Tabs => new TabsComponent(BaseTest.driver);
    }
}
