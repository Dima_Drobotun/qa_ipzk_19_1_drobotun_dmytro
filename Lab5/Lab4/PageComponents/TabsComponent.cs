﻿using OpenQA.Selenium;

namespace Lab5.PageComponents
{
    public class TabsComponent
    {
        private IWebDriver driver;

        public TabsComponent(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement OwnerTab() => driver.FindElement(By.CssSelector(".ownerTab"));
        public IWebElement AllOwnersItem() => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));
        public IWebElement FirstOwnerLink() => driver.FindElement(By.CssSelector("tr:first-of-type > td.ownerFullName > a"));
        public IWebElement AddOwnerItem() => driver.FindElement(By.CssSelector(".open li:nth-child(2) span:nth-child(2)"));
        public IWebElement PetTypeTab() => driver.FindElement(By.CssSelector("li:nth-child(4) > a"));
        public IWebElement SpecialtiesTab() => driver.FindElement(By.CssSelector("li:nth-child(5) span:nth-child(2)"));
        public IWebElement VeterinariansTab() => driver.FindElement(By.CssSelector(".vetsTab"));
        public IWebElement VeterinariansAllItem() => driver.FindElement(By.CssSelector(".open li:nth-child(1) span:nth-child(2)"));
        public IWebElement VeterinariansAddItem() => driver.FindElement(By.CssSelector(".open li:nth-child(2) > a"));

        public void OpenAllOwnersPage()
        {
            OwnerTab().Click();
            AllOwnersItem().Click();
        }

        public void OpenFirstIwnerInfoPage()
        {
            FirstOwnerLink().Click();
        }

        public void OpenAddOwnerPage()
        {
            OwnerTab().Click();
            AddOwnerItem().Click();
        }

        public void OpenPetTypesAllPage()
        {
            PetTypeTab().Click();
        }

        public void OpenSpecialtiesPage()
        {
            SpecialtiesTab().Click();
        }

        public void OpenVeterinariansAllPage()
        {
            VeterinariansTab().Click();
            VeterinariansAllItem().Click();
        }

        public void OpenVeterinarianAddPage()
        {
            VeterinariansTab().Click();
            VeterinariansAddItem().Click();
        }
    }
}
