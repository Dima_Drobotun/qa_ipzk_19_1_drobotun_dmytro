// Generated by Selenium IDE
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
[TestFixture]
public class TestOwnerInformationTest {
  private IWebDriver driver;
  public IDictionary<string, object> vars {get; private set;}
  private IJavaScriptExecutor js;
  [SetUp]
  public void SetUp() {
    driver = new ChromeDriver();
    js = (IJavaScriptExecutor)driver;
    vars = new Dictionary<string, object>();
  }
  [TearDown]
  protected void TearDown() {
    driver.Quit();
  }
  [Test]
  public void testOwnerInformation() {
    driver.Navigate().GoToUrl("http://20.50.171.10:8080/");
    driver.Manage().Window.Size = new System.Drawing.Size(1366, 728);
    driver.FindElement(By.CssSelector(".ownerTab")).Click();
    driver.FindElement(By.CssSelector(".open li:nth-child(1) > a")).Click();
    driver.FindElement(By.LinkText("John Lemon")).Click();
    {
      WebDriverWait wait = new WebDriverWait(driver, System.TimeSpan.FromSeconds(30));
      wait.Until(driver => driver.FindElements(By.CssSelector("b")).Count > 0);
    }
    driver.Close();
  }
}
